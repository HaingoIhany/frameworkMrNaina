/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Vector;
import utilitaire.Connexion;

/**
 *
 * @author haingo
 */
public class Employe {
    Integer id;
    String nom;
    String prenom;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }
    
    
    public void insert(){
        Connexion con=new Connexion();
        try{
            PreparedStatement stmt=con.getCON().prepareStatement("insert into Employe values(null,?,?)");
            stmt.setString(1,getNom());
            stmt.setString(2,getPrenom());
            int res = stmt.executeUpdate(); 
        }
        catch(Exception e)
        {
            
        }finally{
            con.close();
        }
    }
        
    public Vector<Employe> findAll(){
        Vector<Employe> ls= new Vector<Employe>();
        Connexion con=new Connexion();
        try{
            PreparedStatement stmt=con.getCON().prepareStatement("SELECT * FROM Employe");
            ResultSet res = stmt.executeQuery();
            while(res.next()){
                Employe emp=new Employe(res.getInt("id"),res.getString("nom"),res.getString("prenom"));
                ls.add(emp);
            }   
        }catch(Exception e){
            e.printStackTrace();
        }finally{
            con.close();
        }       
        return ls;
    }

    public Employe() {
    }

    public Employe(String nom, String prenom) {
        this.nom = nom;
        this.prenom = prenom;
    }

    public Employe(Integer id, String nom, String prenom) {
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
    }
  
    
}
