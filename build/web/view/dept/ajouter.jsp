<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Insertion d'employe</h1>
        <form name="form1" method="get" action="${pageContext.request.contextPath}/go">
            <p>Nom : <input type="text" name="emp.nom"></p>
            <p>Prénom : <input type="text" name="emp.prenom"></p>
            <input type="submit" name="Submit" value="Soumettre">
        </form>
        <a href="${pageContext.request.contextPath}/Dept/lister.do" class="btn btn-primary btn-lg disabled" role="button" >Liste Employe</a>
    </body>
</html>
