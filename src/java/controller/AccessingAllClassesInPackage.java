/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;


import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Set;
import java.util.Vector;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;
import myAnnotation.Annotation.ANNOTATION;

/**
 *
 * @author haingo
 */
public class AccessingAllClassesInPackage {

    public Vector<Class> allClasse(HttpServletRequest req) throws Exception{
        Vector<Class> valiny =new Vector<Class>();
        String path = req.getServletContext().getRealPath("/")+"WEB-INF\\classes\\controller";
        path =path.replace("\\", "/");
        try{
            File dir =new File(path);
            File[] liste =dir.listFiles();
            for(int i =0;i<liste.length;i++){
                Class temp =Class.forName("controller."+liste[i].getName().substring(0, liste[i].getName().lastIndexOf(".")));
                valiny.add(temp);
            }
            return valiny;
 
        }catch(Exception e){
            throw e;
        }
    }

    public ArrayList<Method> allMethodeAnnoted(HttpServletRequest req) throws Exception {
        ArrayList<Method> valiny = new ArrayList<Method>();
        Vector<Class> classes =this.allClasse(req);
       
        for (Class a : classes) {
            //System.out.println(a.getSimpleName());
            Method[] alMethode = a.getMethods();
            for (int i = 0; i < alMethode.length; i++) {
                if (alMethode[i].isAnnotationPresent(ANNOTATION.class)) {
                    valiny.add(alMethode[i]);
                }
            }
        }
        return valiny;
    }
    public static void main (String[] arg) throws Exception{
        FrontController f=new FrontController();
        AccessingAllClassesInPackage a =new AccessingAllClassesInPackage();
        //ArrayList<Method> arr= a.allMethodeAnnoted(req);
        
    }

}
