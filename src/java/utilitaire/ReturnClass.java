/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilitaire;

import java.util.HashMap;

/**
 *
 * @author haingo
 */
public class ReturnClass {
    HashMap<String,Object> data;
    String view;
    
    
    public ReturnClass(HashMap<String, Object> data, String view) {
        this.data = data;
        this.view = view;
    }

    public ReturnClass() {
    }
    
    public HashMap<String, Object> getData() {
        return data;
    }

    public void setData(HashMap<String, Object> data) {
        this.data = data;
    }

    public String getView() {
        return view;
    }

    public void setView(String view) {
        this.view = view;
    }
    
    
    
}
