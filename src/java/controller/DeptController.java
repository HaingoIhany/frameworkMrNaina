/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.HashMap;
import modele.Employe;
import myAnnotation.Annotation.ANNOTATION;
import myAnnotation.Url.UrlMapping;
import utilitaire.ReturnClass;

/**
 *
 * @author haingo
 */
public class DeptController {
    Employe emp;

    public Employe getEmp() {
        return emp;
    }

    public void setEmp(Employe emp) {
        this.emp = emp;
    }
    
    @UrlMapping(annotation="list",url="Dept/lister")
    public ReturnClass lister(){
        HashMap<String,Object> map=new HashMap<String,Object>();
        Employe temp=new Employe();
        map.put("listeEmploye",temp.findAll());
        ReturnClass ret=new ReturnClass();
        ret.setData(map);
        ret.setView("view/dept/lister.jsp");
        return ret;
    }
    
    @ANNOTATION(methodClasse = "go", url = "Dept/ajouter")
    public ReturnClass ajouter(){
        emp.insert();
        ReturnClass ret=new ReturnClass();
        ret.setView("list");
        return ret;
    }
    
    
}
