/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import myAnnotation.Url.UrlMapping;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import myAnnotation.Url;
import utilitaire.ReturnClass;

/**
 *
 * @author Haingo
 */
public class FrontController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    ArrayList<Method> annotationList;
    String marqueur = ".do";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String servletpath = request.getServletPath();
        if (servletpath.indexOf(".do") != -1) {
            String chemin = servletpath.substring(1, servletpath.length() - marqueur.length());
            urlDo(request, response, chemin);
        } else {
            try {
                //String urlMapping = getUrl(servletpath.substring(1),request);
                //out.println(urlMapping);
                AccessingAllClassesInPackage a =new AccessingAllClassesInPackage();
                ArrayList<Method> met =a.allMethodeAnnoted(request);
                for(Method m:met){
                    out.println(m.getName());
                }
                
                // urlDo(request, response, urlMapping);
            } catch (Exception ex) {
                Logger.getLogger(FrontController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    public void urlDo(HttpServletRequest request, HttpServletResponse response, String chemin) {
        if (chemin.indexOf("/") != -1) {
            String nomClass = chemin.split("/")[0];
            String nomMethod = chemin.split("/")[1];
            try {
                Class classController = Class.forName("controller." + nomClass + "Controller");
                Method classMethod = classController.getMethod(nomMethod);
                Object objet = classController.newInstance();
                Enumeration<String> liste = request.getParameterNames();
                Field[] attributs = classController.getDeclaredFields();
                for (; liste.hasMoreElements();) {
                    String paramName = liste.nextElement();
                    boolean a = paramName.contains(".");
                    if (a == true) {
                        String[] tab = paramName.split("\\.");
                        String attr = tab[0];
                        String attrDonne = tab[1];
                        for (int i = 0; i < attributs.length; i++) {
                            if (attr.compareTo(attributs[i].getName()) == 0) {
                                Method m = classController.getMethod("get" + toFirstUpperCase(attr));
                                Object attrObject = m.invoke(objet);
                                Field donne = attributs[i].getType().getDeclaredField(attrDonne);
                                Method methodSetDonne = attributs[i].getType().getDeclaredMethod("set" + toFirstUpperCase(attrDonne), donne.getType());
                                if (attrObject == null) {
                                    Object attrInstance = attributs[i].getType().newInstance();
                                    methodSetDonne.invoke(attrInstance, request.getParameter(paramName));
                                    Method methodSetController = classController.getMethod("set" + toFirstUpperCase(attr), attributs[i].getType());
                                    methodSetController.invoke(objet, attrInstance);
                                } else {
                                    methodSetDonne.invoke(attrObject, request.getParameter(paramName));
                                }
                                break;
                            }
                        }
                    } else {
                        for (int i = 0; i < attributs.length; i++) {
                            if (paramName.compareTo(attributs[i].getName()) == 0) {
                                Method methodSetController = classController.getMethod("set" + toFirstUpperCase(paramName), attributs[i].getType());
                                methodSetController.invoke(objet, request.getParameter(paramName));
                            }
                        }
                    }
                }
                if (classMethod.getReturnType().equals(Void.TYPE)) {
                    classMethod.invoke(objet);
                } else {
                    ReturnClass cls = (ReturnClass) classMethod.invoke(objet);
//                    String view="/view/"+nomClass.toLowerCase()+"/"+nomMethod+".jsp";
                    String view = "/";
                    if (cls.getView() != null) {
                        view = view.concat(cls.getView());
                    } else {
                        view = view.concat(nomClass.toLowerCase() + "/" + nomMethod + ".jsp");
                    }
                    RequestDispatcher disp = request.getRequestDispatcher(view);
                    if (cls.getData() != null) {
                        for (Map.Entry dt : cls.getData().entrySet()) {
                            request.setAttribute((String) dt.getKey(), dt.getValue());
                        }
                    }
                    disp.forward(request, response);
                }
            } catch (ClassNotFoundException e) {

            } catch (Exception e) {
            }

        }
    }

    public Vector<Class> findAllClasses(String packageName) {
        Vector<Class> rep = new Vector<Class>();
        try {
            String root = getServletContext().getRealPath("/").replace("\\", "/");
            File directoryClass = new File(root.concat("WEB-INF/classes/").concat(packageName));
            File[] liste = directoryClass.listFiles();
            for (int i = 0; i < liste.length; i++) {
                Class temp = Class.forName(packageName + "."
                        + liste[i].getName().substring(0, liste[i].getName().lastIndexOf('.')));
                rep.add(temp);
            }
        } catch (Exception e) {

        }
        return rep;
    }

    public void setAnnotationList(HttpServletRequest req) {
        try {
            AccessingAllClassesInPackage a = new AccessingAllClassesInPackage();
            this.annotationList = a.allMethodeAnnoted(req);
        } catch (Exception ex) {
            Logger.getLogger(FrontController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public ArrayList<Method> getAnnotationList() {
        return annotationList;
    }

    public String getUrl(String keyWordreq, HttpServletRequest req) {
        String url = null;
        if (getAnnotationList() == null) {
            setAnnotationList(req);
        }
        for (Method met : getAnnotationList()) {
            if (keyWordreq.compareTo((String)met.getAnnotation(Url.UrlMapping.class).annotation() ) == 0)  {
                   url=(String)met.getAnnotation(Url.UrlMapping.class).url();
            }

        }
        return url;
    }

    public String toFirstUpperCase(String word) {
        if (word == null) {
            return null;
        } else if (word.length() == 1) {
            return word.toUpperCase();
        } else {
            String ret = word.substring(0, 1).toUpperCase() + word.substring(1);
            return ret;
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
